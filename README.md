## Запуск

- создать .env файл по примеру ниже
- запуск базы: `npm run docker`
- применение миграции: `npm run deploy:db`
- запуск бэка: `npm run start:dev`
- документация будет на роуте: `http://localhost:3005/swagger`

## Пример .env файла

```
COMPOSE_PROJECT_NAME=auth

# Postgres

POSTGRES_HOST=localhost
POSTGRES_PORT=5433
POSTGRES_USER=posrgres
POSTGRES_PASSWORD=posrgres
POSTGRES_DB_NAME=backend

# Dubplicate connectionData

DATABASE_URL="postgresql://posrgres:posrgres:5433/backend?schema=public"

# JWT

JWT_SECRET=qelkweklqke21lk1ok212ke10212kj12k0e12021ek120e
JWT_TOKEN_AUDIENCE=localhost:3005
JWT_TOKEN_ISSUER=localhost:3005
JWT_TOKEN_ACCESS_TTL=3600
JWT_REFRESH_TOKEN_TTL=86400

# Лимит на отправку – 2000 сообщений в день от гугла

EMAIL_HOST=smtp.gmail.com
EMAIL_USER=test@gmail.com
EMAIL_PASS=asdkasdlsaklsdklksd
EMAIL_FROM=Test mail <test@gmail.com>
```
