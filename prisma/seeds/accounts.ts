import { Account, PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";

const testEmail = "test@gmail.com";
const testPassword = "Qwerty123!";

export const createAccounts = async (prisma: PrismaClient): Promise<Account> => {
	const user = await prisma.account.findUnique({
		where: {
			email: testEmail,
		},
	});

	if (!user) {
		const testUser = await prisma.account.create({
			data: {
				email: testEmail,
				password: testPassword,
				firstName: faker.person.firstName(),
				lastName: faker.person.lastName(),
			},
		});

		console.log("Account created: ", testUser);
		return testUser;
	}

	console.log("Account already exists: ", user);
	return user;
};
