import { PrismaClient } from "@prisma/client";
import { createAccounts } from "./seeds/accounts";

const prisma = new PrismaClient();

async function main() {
	console.log("Start seeding ...");
	await createAccounts(prisma);
}

main()
	.catch((e) => {
		throw e;
	})
	.finally(async () => {
		await prisma.$disconnect();
	});
