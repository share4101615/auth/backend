import { ConfigModule, ConfigService, ConfigType } from "@nestjs/config";
import { MailerAsyncOptions } from "@nestjs-modules/mailer/dist/interfaces/mailer-async-options.interface";
import { HandlebarsAdapter } from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";
import { globalConfig } from "./global.config";
import { join } from "path";

export function getEmailConfig(): MailerAsyncOptions {
	return {
		useFactory: (configService: ConfigService) => {
			const config: ConfigType<typeof globalConfig> = configService.get("global");
			const { from, host, pass, user } = config.email;
			return {
				transport: {
					host: host,
					secure: true,
					auth: {
						user: user,
						pass: pass,
					},
				},
				defaults: {
					from: from,
				},
				template: {
					dir: join(__dirname, "email/templates"),
					adapter: new HandlebarsAdapter(),
					options: {
						strict: true,
					},
				},
			};
		},
		inject: [ConfigService],
		imports: [ConfigModule],
	};
}
