import * as Joi from "@hapi/joi";
import { registerAs } from "@nestjs/config";

export const validationSchema = Joi.object({
	// // APP
	// APP_MODE: Joi.string().valid("development", "production").required(),
	// APP_NAME: Joi.string().required(),
	// APP_PORT: Joi.number().default(3000),

	// SSL_KEY_PATH: Joi.string().required(),
	// SSL_CERT_PATH: Joi.string().required(),
	// JWT
	JWT_SECRET: Joi.string().required(),
	JWT_TOKEN_AUDIENCE: Joi.string().required(),
	JWT_TOKEN_ISSUER: Joi.string().required(),
	JWT_TOKEN_ACCESS_TTL: Joi.number().default(3600),
	JWT_TOKEN_REFRESH_TTL: Joi.number().default(86400),

	// Redis
	// REDIS_HOST: Joi.string().default("localhost"),
	// REDIS_PORT: Joi.number().default(6379),
});

export const globalConfig = registerAs("global", () => ({
	// app: {
	// 	name: process.env.APP_NAME,
	// 	mode: process.env.APP_MODE,
	// },
	jwt: {
		secret: process.env.JWT_SECRET,
		audience: process.env.JWT_TOKEN_AUDIENCE,
		issuer: process.env.JWT_TOKEN_ISSUER,
		accessTtl: parseInt(process.env.JWT_TOKEN_ACCESS_TTL, 10),
		refreshTtl: parseInt(process.env.JWT_TOKEN_REFRESH_TTL, 10),
	},
	email: {
		host: process.env.EMAIL_HOST,
		user: process.env.EMAIL_USER,
		pass: process.env.EMAIL_PASS,
		from: process.env.EMAIL_FROM,
	},
	// redis: {
	// 	host: process.env.REDIS_HOST,
	// 	port: parseInt(process.env.REDIS_PORT, 10),
	// },
	// ssl: {
	// 	keyPath: process.env.SSL_KEY_PATH,
	// 	certPath: process.env.SSL_CERT_PATH,
	// },
}));
