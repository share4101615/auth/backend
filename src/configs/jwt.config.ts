import { ConfigModule, ConfigService, ConfigType } from "@nestjs/config";
import { JwtModuleAsyncOptions } from "@nestjs/jwt";

import { globalConfig } from "./global.config";

export function getJwtConfig(): JwtModuleAsyncOptions {
	return {
		useFactory: (configService: ConfigService) => {
			const config: ConfigType<typeof globalConfig> = configService.get("global");

			return {
				secret: config.jwt.secret,
				audience: config.jwt.audience,
				issuer: config.jwt.issuer,
				accessTokenTtl: config.jwt.accessTtl,
				refreshTokenTtl: config.jwt.refreshTtl,
			};
		},
		inject: [ConfigService],
		imports: [ConfigModule],
	};
}
