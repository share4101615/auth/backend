import { IGetAccount, IJWTTokenData } from "@nbelous/share-auth";
import { AccountsService } from "./accounts.service";
import { Controller, Get } from "@nestjs/common";
import { ActiveUser } from "src/auth/decorators/active-user.decorator";

@Controller("accounts")
export class AccountsController {
	constructor(private readonly accountsService: AccountsService) {}
	@Get()
	async getAccount(@ActiveUser() jwt: IJWTTokenData): Promise<IGetAccount> {
		const account = await this.accountsService.findById(jwt.sub);
		if (account) {
			delete account.password;
		}
		return account;
	}
}
