import { PrismaModule } from "src/prisma/prisma.module";
import { AccountsService } from "./accounts.service";
import { Module } from "@nestjs/common";
import { AccountsController } from "./accounts.controller";

@Module({
	imports: [PrismaModule],
	providers: [AccountsService],
	exports: [AccountsService],
	controllers: [AccountsController],
})
export class AccountsModule {}
