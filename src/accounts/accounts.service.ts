import { ConflictException, Injectable } from "@nestjs/common";
import { Account, Prisma } from "@prisma/client";
import { PrismaService } from "src/prisma/prisma.service";

@Injectable()
export class AccountsService {
	constructor(private readonly prisma: PrismaService) {}

	public findByEmail(email: string): Promise<Account | null> {
		return this.prisma.account.findUnique({
			where: {
				email,
			},
		});
	}

	public findById(id: number): Promise<Account | null> {
		return this.prisma.account.findUnique({
			where: {
				id,
			},
		});
	}

	public async create(data: Prisma.AccountCreateInput): Promise<Account> {
		try {
			return await this.prisma.account.create({
				data,
			});
		} catch (error) {
			throw new ConflictException("Account already exists");
		}
	}

	public async updateById(id: number, data: Prisma.AccountUpdateInput): Promise<Account> {
		try {
			return await this.prisma.account.update({
				where: {
					id,
				},
				data,
			});
		} catch (error) {
			throw new Error("Not updated");
		}
	}

	async deleteById(id: number): Promise<Account> {
		return this.prisma.account.delete({
			where: {
				id,
			},
		});
	}
}
