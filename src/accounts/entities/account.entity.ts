import { ApiProperty } from "@nestjs/swagger";
import { Account } from "@prisma/client";

export class AccountEntity implements Account {
	@ApiProperty()
	id: number;

	@ApiProperty()
	email: string;

	@ApiProperty()
	password: string;

	@ApiProperty()
	createdAt: Date;

	@ApiProperty()
	updatedAt: Date;

	@ApiProperty({ required: false, nullable: true })
	firstName: string | null;

	@ApiProperty({ required: false, nullable: true })
	lastName: string | null;

	@ApiProperty({ required: false, nullable: true })
	companyName: string | null;

	@ApiProperty({ required: false, nullable: true })
	companyDirection: string | null;

	@ApiProperty({ required: false, nullable: true })
	companyPepole: string | null;
}
