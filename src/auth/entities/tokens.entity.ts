import { ITokens } from "@nbelous/share-auth";
import { ApiProperty } from "@nestjs/swagger";

export class TokensEntity implements ITokens {
	@ApiProperty()
	accessToken: string;
	@ApiProperty()
	refreshToken: string;
}
