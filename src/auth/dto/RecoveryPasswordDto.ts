import { IRecoveryPassword } from "@nbelous/share-auth";
import { ApiProperty } from "@nestjs/swagger";
import { IsEmail } from "class-validator";

export class RecoveryPasswordDto implements IRecoveryPassword {
	@ApiProperty()
	@IsEmail()
	email: string;
}
