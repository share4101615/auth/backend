import { ISignUp } from "@nbelous/share-auth";
import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString, MinLength } from "class-validator";

export class SignUpDto implements ISignUp {
	@ApiProperty()
	@IsString()
	email: string;
	@ApiProperty()
	@IsString()
	@MinLength(6)
	password: string;
	@ApiProperty()
	@IsString()
	@IsOptional()
	firstName?: string;
	@ApiProperty()
	@IsString()
	@IsOptional()
	lastName?: string;
	@ApiProperty()
	@IsString()
	@IsOptional()
	companyName?: string;
	@ApiProperty()
	@IsString()
	@IsOptional()
	companyDirection?: string;
	@ApiProperty()
	@IsString()
	@IsOptional()
	companyPepole?: string;
}
