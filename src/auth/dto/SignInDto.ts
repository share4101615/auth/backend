import { ISignIn } from "@nbelous/share-auth";
import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class SignInDto implements ISignIn {
	@ApiProperty()
	@IsString()
	email: string;
	@ApiProperty()
	@IsString()
	password: string;
}
