import { AccountsService } from "./../accounts/accounts.service";
import { Inject, Injectable, UnauthorizedException } from "@nestjs/common";
import { compare, hash, genSalt } from "bcrypt";
import { ISignIn, ISignUp, ITokens } from "@nbelous/share-auth";
import { JwtService } from "@nestjs/jwt";
import { ConfigType } from "@nestjs/config";
import { globalConfig } from "src/configs";
import { EmailService } from "src/email/email.service";

@Injectable()
export class AuthService {
	constructor(
		@Inject(globalConfig.KEY)
		private readonly config: ConfigType<typeof globalConfig>,
		private readonly accountsService: AccountsService,
		private readonly jwtService: JwtService,
		private readonly emailService: EmailService,
	) {}

	public async signIn(dto: ISignIn): Promise<ITokens> {
		const account = await this.accountsService.findByEmail(dto.email);
		if (!account) {
			throw new UnauthorizedException("Account not found");
		}
		const isPasswordValid = await this.validatePassword(dto.password, account.password);
		if (!isPasswordValid) {
			throw new UnauthorizedException("Account not found");
		}

		return this.generateTokens(account.id);
	}

	public async signUp(dto: ISignUp): Promise<ITokens> {
		const password = await this.hashPassword(dto.password);
		const account = await this.accountsService.create({
			...dto,
			password,
		});

		if (!account) {
			throw new Error("Account not created");
		}

		return this.generateTokens(account.id);
	}

	public async recoveryPassword(email: string): Promise<void> {
		const account = await this.accountsService.findByEmail(email);
		if (account) {
			const password = this.generatePassword();
			const hashedPassword = await this.hashPassword(password);

			await this.accountsService.updateById(account.id, {
				password: hashedPassword,
			});

			this.emailService.sendREcoveryPasswordMail({
				email,
				password,
			});
		}
	}

	private async hashPassword(password: string): Promise<string> {
		const salt = await genSalt(10);
		return hash(password, salt);
	}

	private async validatePassword(password: string, hashedPassword: string): Promise<boolean> {
		return compare(password, hashedPassword);
	}

	private async generateTokens(accountId: number): Promise<ITokens> {
		const { audience, issuer, secret, accessTtl, refreshTtl } = this.config.jwt;
		const accessToken = await this.jwtService.signAsync(
			{ sub: accountId },
			{
				audience,
				issuer,
				secret,
				expiresIn: accessTtl,
			},
		);
		const refreshToken = await this.jwtService.signAsync(
			{ sub: accountId },
			{
				audience,
				issuer,
				secret,
				expiresIn: refreshTtl,
			},
		);

		return {
			accessToken,
			refreshToken,
		};
	}

	private generatePassword(): string {
		const length = 8;
		const charset = "@#$&*0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@#$&*0123456789abcdefghijklmnopqrstuvwxyz";
		let password = "";
		for (let i = 0, n = charset.length; i < length; ++i) {
			password += charset.charAt(Math.floor(Math.random() * n));
		}
		return password;
	}
}
