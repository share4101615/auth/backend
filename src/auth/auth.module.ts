import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { AccountsModule } from "src/accounts/accounts.module";
import { JwtModule } from "@nestjs/jwt";
import { getJwtConfig } from "src/configs/jwt.config";
import { AuthenticationGuard } from "./guards/authentication/authentication.guard";
import { APP_GUARD } from "@nestjs/core";
import { AccessTokenGuard } from "./guards/access-token/access-token.guard";
import { EmailModule } from "src/email/email.module";

@Module({
	imports: [AccountsModule, JwtModule.registerAsync(getJwtConfig()), EmailModule],
	providers: [
		AuthService,
		{
			provide: APP_GUARD,
			useClass: AuthenticationGuard,
		},
		AccessTokenGuard,
	],
	controllers: [AuthController],
})
export class AuthModule {}
