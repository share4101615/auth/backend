import { Body, Controller, Post } from "@nestjs/common";
import { ITokens } from "@nbelous/share-auth";

import { AuthService } from "./auth.service";
import { SignInDto } from "./dto/SignInDto";
import { SignUpDto } from "./dto/SignUpDto";
import { TokensEntity } from "./entities/tokens.entity";
import { ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { AuthType } from "./enums/auth-type.enum";
import { Auth } from "./decorators/auth.decorator";
import { RecoveryPasswordDto } from "./dto/RecoveryPasswordDto";

@ApiTags("Аутентификация")
@Auth(AuthType.None)
@Controller("auth")
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@ApiOkResponse({ type: TokensEntity })
	@Post("sign-in")
	async signIn(@Body() dto: SignInDto): Promise<ITokens> {
		return this.authService.signIn(dto);
	}

	@ApiOkResponse({ type: TokensEntity })
	@Post("sign-up")
	async signUp(@Body() dto: SignUpDto): Promise<ITokens> {
		return this.authService.signUp(dto);
	}

	@Post("recovery-password")
	async recoveryPassword(@Body() dto: RecoveryPasswordDto): Promise<void> {
		return this.authService.recoveryPassword(dto.email);
	}
}
