import { CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigType } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { Request } from "express";
import { REQUEST_USER_KEY } from "src/auth/auth.constants";
import { globalConfig } from "src/configs";

@Injectable()
export class AccessTokenGuard implements CanActivate {
	constructor(
		private readonly jwtService: JwtService,
		@Inject(globalConfig.KEY)
		private readonly config: ConfigType<typeof globalConfig>,
	) {}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest<Request>();
		const token = this.extractTokenFromHeader(request);
		if (!token) {
			throw new UnauthorizedException("Отсутствует access token");
		}

		try {
			const payload = await this.jwtService.verifyAsync(token, {
				secret: this.config.jwt.secret,
				audience: this.config.jwt.audience,
				issuer: this.config.jwt.issuer,
			});
			request[REQUEST_USER_KEY] = payload;
		} catch (err) {
			throw new UnauthorizedException("Невалидный access token");
		}
		return true;
	}

	private extractTokenFromHeader(request: Request): string | undefined {
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		const [_, token] = request.headers.authorization?.split(" ") ?? [];
		return token;
	}
}
