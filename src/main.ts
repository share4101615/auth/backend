import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.useGlobalPipes(
		new ValidationPipe({
			enableDebugMessages: true,
			whitelist: true,
			transform: true,
		}),
	);

	const config = new DocumentBuilder().setTitle("Auth").setDescription("Auth API description").setVersion("0.1").build();

	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup("swagger", app, document);

	app.enableCors();
	await app.listen(3005);
}
bootstrap();
