import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";

import { PrismaModule } from "./prisma/prisma.module";
import { AccountsModule } from "./accounts/accounts.module";
import { AuthModule } from "./auth/auth.module";
import { globalConfig, validationSchema } from "./configs";
import { EmailModule } from "./email/email.module";

@Module({
	imports: [
		ConfigModule.forRoot({
			validationSchema,
			load: [globalConfig],
			isGlobal: true,
		}),
		PrismaModule,
		AccountsModule,
		AuthModule,
		EmailModule,
	],
	controllers: [],
	providers: [],
})
export class AppModule {}
