import { Module } from "@nestjs/common";
import { MailerModule } from "@nestjs-modules/mailer";

import { EmailService } from "./email.service";
import { getEmailConfig } from "src/configs";

@Module({
	imports: [MailerModule.forRootAsync(getEmailConfig())],
	providers: [EmailService],
	exports: [EmailService],
})
export class EmailModule {}
