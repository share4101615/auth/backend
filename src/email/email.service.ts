import { MailerService } from "@nestjs-modules/mailer";
import { Injectable } from "@nestjs/common";

type Props = {
	email: string;
	password: string;
};

@Injectable()
export class EmailService {
	constructor(private mailerService: MailerService) {}

	async sendREcoveryPasswordMail({ email, password }: Props) {
		await this.mailerService.sendMail({
			to: email,
			// from: '"Support Team" <support@example.com>', // override default from
			subject: "Восстановление пароля",
			template: "./recovery-password", // `.hbs` extension is appended automatically
			context: {
				email,
				password,
			},
		});
	}
}
